FROM        ubuntu:14.10
MAINTAINER  Adam Lawrence <alaw005@gmail.com>

# Set location files as seems to create error otherwise
RUN localedef -i en_NZ -f UTF-8 en_NZ.UTF-8

# Install required apps
RUN apt-get update && apt-get install -y supervisor nano curl


# Install syncthing
RUN curl -s https://syncthing.net/release-key.txt | apt-key add - && \
    echo "deb http://apt.syncthing.net/ syncthing release" | tee /etc/apt/sources.list.d/syncthing-release.list && \
    apt-get update && apt-get install -y syncthing


# Create user to run syncthing as from supervisord. Note, inclusion of environment 
# variables for user/group ID to enable align with host if required
ENV HOST_GID 1001
ENV HOST_UID 118
RUN addgroup --gid $HOST_GID syncthing
RUN adduser --gecos "" --disabled-password --ingroup syncthing --uid $HOST_UID syncthing
RUN echo 'syncthing:vagrant' | chpasswd


## Enable ssh for development ##
#RUN apt-get install -y openssh-server && \
#    mkdir /var/run/sshd && \
#    echo 'root:vagrant' | chpasswd && \
#    sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
#    sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
#ENV NOTVISIBLE "in users profile"
#RUN echo "export VISIBLE=now" >> /etc/profile
#EXPOSE 22
## End ssh section            ##


# Configure start script
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY config/start.sh /home/syncthing/start.sh
RUN chmod +x /home/syncthing/start.sh


EXPOSE 22000 21025/udp 8384

CMD ["/home/syncthing/start.sh"]

