# Requirements

Requires docker and docker-compose to be installed (I think docker-compose might come with docker in Ubuntu now).

# Install

## Download and configure docker image/container:

    git clone git@bitbucket.org:alaw005/docker-syncthing.git
    cd docker-syncthing

The following settings can be changed in docker-compose.yml file:

### Data volume (datasyncthing)

The data volume can be mapped to the host, for example:

    datasyncthing:
      ...
      volumes:
        - /[host]/[data]/[path]:/mnt/data
        - ../[host relative path]:/home/syncthing/config

Note that in docker-compose relative paths can be used.

### User and group ids (UID/GID)

UID/GID can be set to to match host. This would be desirable if sharing folders on the host (rather 
than purely through the data container). The syncthing container creates a user/group called 
syncthing/syncthing to use internally but this can be made to match any host user/group by setting 
environment variables.

Find UID and GID on host:

    id -u [username]
    id -g [groupname]

Then specify values in docker compose docker-compose.yml like this:

    ...
    syncthing:
      ...
      environment:
        - HOST_UID: 118
        - HOST_GID: 1001
      ...


## Build docker image

First, make sure you are in to the docker-syncthing project folder (with docker-compose.yml file).

To build docker image:

    sudo docker-compose build

# Usage

First, make sure you are in to the docker-syncthing project folder (with docker-compose.yml file).

To run docker container:

    sudo docker-compose up -d

To view logs in real time:

    sudo docker-compose logs

You need the IP address to access the web GUI. To get IP address (note project name e.g. 
dockersyncthing_syncthing_1 can be found in logs):

    sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' dockersyncthing_syncthing_1

You can now access GUI:

    http://[IP address from above]:8384

You should now change access to ssl and set login name in settings
section of web interface.

# Final word

Thank you to others whose work has inspired this:

* https://github.com/joeybaker/docker-syncthing
* http://www.webupd8.org/2015/05/official-syncthing-debian-ubuntu.html
* https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-syncthing-to-synchronize-directories-on-ubuntu-14-04

