#!/bin/bash

# Add any additional tasks here to run once on container start
# before launching supervisord

# Make sure user syncthing owns everything in home directory
# this is necessary here because binding volume which likely
# owned by root. Volume is for config settings
chown -R syncthing:syncthing /home/syncthing

# Create syncthing config if not already created
if [ ! -f /home/syncthing/.config/syncthing/config.xml ]; then
su syncthing <<'EOF'
/usr/bin/syncthing -generate="/home/syncthing/.config/syncthing"
sed -i "s/<address>.*:8384<\/address>/<address>0.0.0.0:8384<\/address>/" /home/syncthing/.config/syncthing/config.xml
EOF
fi

# Start supervisord
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf

